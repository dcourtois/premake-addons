# Moved to Github !

Due to Premake's main repository moving to Github, those 2 addons are now also moved there. I also split them, and here are the addresses :

 * [Compilation Unit module](https://github.com/dcourtois/premake-compilationunit)
 * [Qt module](https://github.com/dcourtois/premake-qt)